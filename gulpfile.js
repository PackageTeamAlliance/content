var elixir = require('laravel-elixir');

elixir(function(mix) {

    mix
        .browserify([
            'components/content-script.js',

        ], './resources/assets/js/bundle/content-script.js')
      
        .scripts([
            '../../../node_modules/vue/dist/vue.js',
            '../../../node_modules/vue-resource/dist/vue-resource.js',
            '../../../node_modules/selectize/dist/js/standalone/selectize.js',
            'slugify.js'
        

        ], './resources/assets/modules/content/content.js')

        .scripts([
            'bundle/content-script.js'

        ], './resources/assets/modules/content/content-index.js')
   
        .styles([
            '../../../node_modules/selectize/dist/css/selectize.bootstrap3.css'
    
        ], './resources/assets/modules/content/content.css')
});
