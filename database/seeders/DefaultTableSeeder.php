<?php

namespace Modules\Content\Database\Seeders;

use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('contents')->where('slug', 'site-name')->first()) {
            $this->createContent();
        }


    }

    public function createContent()
    {
        DB::table('contents')->insert([
            'name' => 'Site Name',
            'slug' => 'site-name',
            'type' => 'database',
            'type' => 'database',
            'html' => 0,
            'value' => "Core Bootstrap",
            'enabled' => 1,
            'updated_at' => new Datetime,
            'created_at' => new Datetime,
        ]);
    }
}
