<?php

namespace Pta\Content\Facades;

use Illuminate\Support\Facades\Facade;

class ContentFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'core.content';
    }
}
