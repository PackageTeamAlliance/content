<?php

$router->group(['namespace' => 'Pta\Content\Http\Controllers\Admin'], function () use ($router) {
    
    $router->get('/', ['as' => 'content.dashboard.index', 'uses' => 'ContentController@index']);
    $router->get('table', ['as' => 'content.dashboard.table', 'uses' => 'ContentController@table']);

    $router->get('create', ['as' => 'content.dashboard.create', 'uses' => 'ContentController@create']);
    $router->post('create', ['as' => 'content.dashboard.create.process', 'uses' => 'ContentController@store']);
    
    $router->get('{id}/copy', ['as' => 'content.dashboard.copy', 'uses' => 'ContentController@copy']);
    $router->post('{id}/copy', ['as' => 'content.dashboard.copy.process', 'uses' => 'ContentController@store']);

    $router->get('{id}/edit', ['as' => 'content.dashboard.edit', 'uses' => 'ContentController@edit']);
    $router->post('{id}/edit', ['as' => 'content.dashboard.edit.process', 'uses' => 'ContentController@update']);

    $router->get('{id}/translate', ['as' => 'content.dashboard.translate', 'uses' => 'ContentController@translate']);
    $router->post('{id}/translate', ['as' => 'content.dashboard.translate.process', 'uses' => 'ContentController@translate_store']);

    $router->get('{id}/translations', ['as' => 'content.dashboard.translations', 'uses' => 'ContentController@translations']);

    $router->get('edit/{content_id}/translate/{id}', ['as' => 'content.dashboard.translate.edit', 'uses' => 'ContentController@translate_edit']);
    $router->post('edit/{content_id}/translate/{id}', ['as' => 'content.dashboard.translate.edit.process', 'uses' => 'ContentController@translate_edit_process']);

    $router->post('update/', ['as' => 'content.dashboard.update', 'uses' => 'ContentController@api_update']);
    
    $router->post('{id}/delete',   ['as' => 'content.dashboard.delete', 'uses' => 'ContentController@delete']);
});