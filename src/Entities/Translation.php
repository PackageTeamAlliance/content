<?php

namespace Pta\Content\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Translation extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $table = 'content_translations';
    
    /**
     * {@inheritdoc}
     */
    protected $fillable = ['value', 'locale', 'content_id', 'content_locale', ];
    
    protected $with = [];

    public function translation()
    {
        return $this->hasOne('Pta\Content\Entities\Content');
    }
}
