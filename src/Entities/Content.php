<?php

namespace Pta\Content\Entities;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'contents';
    
    /**
     * {@inheritDoc}
     */
    protected $guarded = ['id'];
    
    /**
     * {@inheritDoc}
     */
    protected $with = ['translation'];


    public function translation()
    {
        return $this->hasMany('Pta\Content\Entities\Translation');
    }
}
