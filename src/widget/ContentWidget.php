<?php

namespace Pta\Content\Widget;

use Illuminate\Container\Container;
use Pta\Content\Repositories\ContentRepository;

class ContentWidget
{
    /**
     * The App container.
     *
     * @var \Illuminate\Container\Container
     */
    protected $app;
    
    /**
     * The Content repository.
     *
     * @var \Pta\Content\Repositories\ContentRepositoryInterface
     */
    protected $content;
    
      /**
     * The Current Locale.
     *
     * @return String
     */
    protected $locale;
    
    public function __construct(ContentRepository $content)
    {
        $this->content = $content;

        $this->locale = app()->getLocale();
    }
    
    public function show($slug)
    {
        $content = $this->content->render($slug, $this->locale);
        if ($content) {
            return $content;
        }

        return $slug;
    }
}
