<?php

namespace Pta\Content\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Pta\Content\Http\Controllers\Controller;
use Pta\Content\Repositories\ContentRepository;
use Pta\Content\Http\Requests\EditContentRequest;
use Pta\Content\Repositories\TranslationRepository;
use Pta\Content\Http\Requests\CreateContentRequest;
use Pta\Content\Http\Requests\DeleteContentRequest;
use Pta\Content\Http\Requests\EditTranslationRequest;
use Pta\Content\Http\Requests\CreateTranslationRequest;

class ContentController extends Controller
{
    /**
     * The Content repository.
     *
     * @var \Pta\Content\Repositories\ContentRepository
     */
    protected $content;

    /**
     * The Translation repository.
     *
     * @var \Pta\Content\Repositories\TranslationRepository
     */
    protected $translation;

    /**
     * The Filesystem Manager.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $manager;

    /**
     * The Fields required to build the table.
     *
     * @var Array
     */
    protected $fields = ['id','name','slug', 'type', 'html', 'enabled'];
    /**
     * Constructor.
     *
     * @param  \Pta\Content\Repositories\ContentRepositoryInterface $contents
     * @return void
     */
    public function __construct(ContentRepository $content, TranslationRepository $translation, Filesystem $manager)
    {
        $this->content = $content;

        $this->translation = $translation;

        $this->manager = $manager;
    }

    public function table(Request $request)
    {
        return $this->content->get($request->all(), $this->fields);
    }

    public function index(Request $request)
    {
        return $this->getView('admin.dashboard.index');
    }

    public function create()
    {
        return $this->showForm('create');
    }

    public function copy($id)
    {
        return $this->showForm('copy', $id);
    }

    public function store(CreateContentRequest $request, $id = null)
    {
        $store = $this->content->create($request->except(['files', 'content_id']));

        list($status, $instance) = $store;

        return redirect()->route('content.dashboard.edit', [$instance->id])->withFlashSuccess(trans("pta/content::message.success.create"));
    }

  
    public function api_update(Request $request)
    {
        $this->content->toggleStatus($request->get('pk'));
        
        return http_response_code(200);
    }

    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    public function update($id, EditContentRequest $request)
    {
        $update = $this->content->update($id, $request->except(['files', 'content_id']));

        list($status, $instance) = $update;

        return redirect()->route('content.dashboard.edit', [$instance->id])->withFlashSuccess(trans("pta/content::message.success.update"));
    }

    public function translate($id)
    {
        return $this->showTranslateForm('translate', $id, null);
    }

    public function translations($id)
    {
        return $this->showTranslateForm('translate', $id, null);
    }

    public function translate_edit($content_id, $id)
    {
        return $this->showTranslateForm('edit', $content_id, $id);
    }

    public function translate_store($id, CreateTranslationRequest $request)
    {
        $this->translation->create($request->except(['files']));

        return redirect()->route('content.dashboard.index')->withFlashSuccess(trans("pta/content::message.success.create"));
    }

    public function translate_edit_process($content_id, $id, EditTranslationRequest $request)
    {
        $this->translation->update($id, $request->except(['files']));

        return redirect()->route('content.dashboard.index')->withFlashSuccess(trans("pta/content::message.success.create"));
    }

    public function delete(DeleteContentRequest $request, $id)
    {
        $type = $this->content->delete($id) ? 'success' : 'error';
        
        if ($request->ajax() && $type === 'success') {
            return http_response_code(200);
        }

        return redirect()->route('content.dashboard.index')->withFlashSuccess(trans("pta/content::message.{$type}.delete"));
    }

    protected function showTranslateForm($mode, $content_id, $id = null)
    {
        if (isset($id)) {
            if (!$translation = $this->translation->find($id)) {
                return redirect()->route('content.dashboard.index')->withFlashDanger(trans('pta/content::message.not_found', compact('id')));
            }
        } else {
            $translation = $this->translation->createModel();
        }

        $collection = collect(config('languages.locale'));

        $locale = $collection->filter(function ($value, $key) {
            return $value['short_name'] != config('languages.default.short_name');

        });
        
        $content = $this->content->find($content_id);

        return $this->getView('admin.dashboard.translate_form', compact('mode', 'content', 'locale', 'translation'));
    }

    protected function showForm($mode, $id = null)
    {
        if (isset($id)) {
            if (!$content = $this->content->find($id)) {
                return redirect()->route('content.dashboard.index')->withFlashDanger(trans('pta/content::message.not_found', compact('id')));
            }
        } else {
            $content = $this->content->createModel();
        }

        $files = $this->manager->allFiles(config('content.file_path'));

        return $this->getView('admin.dashboard.form', compact('mode', 'content', 'files'));
    }
}
