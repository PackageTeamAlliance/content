<?php

namespace Pta\Content\Http\Requests;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;

class EditTranslationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Gate $gate)
    {
        return access()->allow(config('content.permissions.translation-edit.name'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => 'required',
            'locale' => 'required',
            'content_locale' => 'required|unique:content_translations,content_locale,' . $this->get('translation_id'),
        ];
    }
}
