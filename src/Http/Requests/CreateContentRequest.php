<?php

namespace Pta\Content\Http\Requests;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;

class CreateContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Gate $gate)
    {
        return access()->allow( config('content.permissions.create.name') );
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required|alpha_dash|unique:contents',
            'html' => 'required',
            'type' => 'required|in:database,file',
            'value' => 'required_if:type,database',
            'file' => 'required_if:type,file',
        ];
    }
}
