<?php

namespace Pta\Content\Repositories;

use Pta\Content\Entities\Content;
use Illuminate\Container\Container;
use Pta\Support\Traits\VueTableTrait;
use Rinvex\Repository\Repositories\EloquentRepository;

class ContentRepository extends EloquentRepository
{
    use VueTableTrait;
    
    /**
     * The Data handler.
     *
     * @var \Ninjaparade\Content\Handlers\DataHandlerInterface
     */
    protected $data;
    
    /**
     * The Eloquent content model.
     *
     * @var string
     */
    protected $model;
    
    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container  $container
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->setContainer($container)
            ->setModel(\Pta\Content\Entities\Content::class)
            ->setRepositoryId(md5('pta.content.cache'));
    }

    public function store($id, array $input)
    {
        $data = array_except($input, ['content_id']);

        return !$id ? $this->create($data) : $this->update($id, $data);
    }

    public function render($slug, $locale)
    {
        $content = $this->findWhere(['slug', '=', $slug]);
        
        if ($locale === env('APP_LOCALE', 'en')) {
            $return = $content->first();
        } else {
            $return =  $content->translation()->where('locale', $locale)->first();
        }

        if ($return->html) {
            return $return->value;
        } else {
            return strip_tags($return->value);
        }
        
        return '';
    }

    public function toggleStatus($id)
    {
        // Get the content object
        $content = $this->find($id);

        $this->update($id, ['enabled' => !$content->enabled]);

        return response(200);
    }
}
