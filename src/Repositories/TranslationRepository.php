<?php

namespace Pta\Content\Repositories;

use Illuminate\Container\Container;
use Pta\Support\Traits\VueTableTrait;
use Rinvex\Repository\Repositories\EloquentRepository;

class TranslationRepository extends EloquentRepository
{
    use VueTableTrait;
    

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container  $container
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->setContainer($container)
            ->setModel(\Pta\Content\Entities\Translation::class)
            ->setRepositoryId(md5('pta.content.translation.cache'));
    }
}
