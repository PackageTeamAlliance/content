<?php

namespace Pta\Content\Providers;

use Pta\Content\Widget\ContentWidget;
use Illuminate\Foundation\AliasLoader;
use Pta\Content\Facades\ContentFacade;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Pta\Content\Repositories\ContentRepository;

class DataServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
    }
    
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        //register repository
        $this->app->singleton(ContentRepository::class, function ($app) {
            return new ContentRepository($app);
        });
        
        $this->bindFacade();
        
        $this->registerBladeContent();
    }
    
    public function bindFacade()
    {
        $this->app['core.content'] = $this->app->share(function ($app) {
            return new ContentWidget($app[ContentRepository::class]);
        });
        
        $loader = AliasLoader::getInstance();
        
        $loader->alias('Content', ContentFacade::class);
    }
    
    public function registerBladeContent()
    {
        $this->app['blade.compiler']->directive('content', function ($slug) {
            return "<?php echo Content::show({$slug}); ?>";
        });
    }
}
