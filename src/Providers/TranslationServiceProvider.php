<?php

namespace Pta\Content\Providers;

use Illuminate\Support\ServiceProvider;
use Pta\Content\Repositories\TranslationRepository;


class TranslationServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {

    }
    
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(TranslationRepository::class, function ($app) {
            return new TranslationRepository($app);
        });
    }
}
