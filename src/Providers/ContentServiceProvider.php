<?php
namespace Pta\Content\Providers;

use File;
use Illuminate\Support\ServiceProvider;
use Pta\Content\Providers\DataServiceProvider;
use Pta\Content\Providers\TranslationServiceProvider;

class ContentServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    
    /**
     * Providers to register
     *
     * @var array
     */
    protected $providers = [TranslationServiceProvider::class, DataServiceProvider::class, \Rinvex\Repository\Providers\RepositoryServiceProvider::class,  ];
    
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerBindings();
        
        $this->registerConfig();
        
        $this->registerViews();
        
        $this->populateMenus();
        
        $this->registerTranslations();
        
        $this->registerMigration();
        
        $this->registerRoutes();

        $this->registerAssets();


        foreach ($this->providers as $provider) {
            if (! $this->app->bound($provider)) {
                $this->app->register($provider);
            }
        }
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();
    }
    
    public function registerAssets()
    {
        $this->publishes([
          realpath(__DIR__ . '/../../resources/assets/modules') => public_path('module/assets/'),
        ], 'core-assets');
    }
    
    /**
     * Register Bindings in IoC.
     *
     * @return void
     */
    protected function registerBindings()
    {
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $configPath = realpath(__DIR__ . '/../../config/config.php');
        
        $this->publishes([ $configPath => config_path('content.php'), 'config']);
        
        $this->mergeConfigFrom($configPath, 'content');
    }
    
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/content');
        
        $this->publishes([realpath(__DIR__ . '/../../resources/views') => base_path('resources/views/vendor/pta/content'), ]);
    }
    
    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $this->loadTranslationsFrom(realpath(__DIR__ . '/../../resources/lang'), 'pta/content');
    }
    
    public function registerMigration()
    {
        $this->publishes([realpath(__DIR__ . '/../../database/migrations') => database_path('/migrations') ], 'migrations');
    }
    
    public function populateMenus()
    {
    }
    
    public function registerRoutes()
    {
        $router = $this->app['router'];
        
        $prefix = config('content.route_prefix');
        
        $security = $this->app['config']->get('content.security.protected', true);

        if (!$this->app->routesAreCached()) {
            $group = [];
            
            $group['prefix'] = $prefix;
            
            if ($security) {
                $group['middleware'] = config('content.security.middleware');
                $group['can'] = config('content.security.permission_name');
            }
            
            $router->group($group, function () use ($router) {
                
                require realpath(__DIR__ . '/../routes.php');
            });
        }
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
