<?php

namespace Pta\Content\Console\Installers;

use Illuminate\Console\Command;
use Pta\Pulse\Console\Contracts\Installer;

class ContentInstaller implements Installer
{
    /**
     * @var Command
     */
    protected $command;
    
    public function run(Command $command)
    {
        $this->command = $command;

        $this->command->call('module:migrate', ['module' => 'content']);

        $this->command->call('module:seed', ['module' => 'content']);
    }
}
