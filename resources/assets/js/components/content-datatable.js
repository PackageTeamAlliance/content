Vue.component('content-table', {
    template: require('./templates/content/content.html'),
    props: ['data_uri', 'create_uri', 'update_uri'],
    data: function() {
        return {
            columns: [],
            contents: [],
            data_uri: '',
            update_uri: '',
            filterKey: '',
            create_uri: '',
            searchKey: ''
        }
    },
    methods: {
        show: function() {

            this.$els.progress.style.display = 'none';
            this.$els.table.style.display = 'table';
            var _self = this;

            setTimeout(function() {
                addDeleteForms();
                $('a.editable').editable({
                    mode: 'popup',
                    showbuttons: true,
                    savenochange: true,
                    url: _self.update_uri,
                    source: [{
                        value: 0,
                        text: 'Draft'
                    }, {
                        value: 1,
                        text: 'Published'
                    }]
                });
            }, 1000);
        }
    },
    compiled: function() {

    },
    ready: function() {
        // GET request

        this.$http.get(this.data_uri, function(data, status, request) {
            // set data on vm
            this.columns = data.columns;
            this.contents = data.content;
            //show that shit.
            this.show();

        }).error(function(data, status, request) {
            // handle error
        });
    }
});

new Vue({
    el: "#content-table"
});
