import Vue from 'vue';
var moment = require('moment');
var VueTables = require('vue-tables');
var resource = require('vue-resource');
var editable = require('./mixins/editable.js');
var deleteable = require('./mixins/deleteable.js');

Vue.use(resource);
Vue.use(VueTables.server, {
  compileTemplates: true,
  highlightMatches: true,
  filterByColumn: false,
  texts: {
    filter: "Search : "
  },
  datepickerOptions: {
    showDropdowns: false
  }
});

new Vue({
  el: "#content-table",
  mixins: [deleteable, editable],
  component : [],

  methods : {

    deletePage : function(event)
    { 
      this.deleteEntry(event.target);
    },

    togglePageStatus : function(event)
    {
      this.toggleStatus(event.target)
    }
  },
  computed: {},
  data: {
    columns:['id','name','slug', 'type', 'html', 'enabled'],
    row: {},
    options: {
      headings: {
        id: 'id',
        name: 'Name',
        slug: 'Slug',
        type: 'Type',
        html: 'Format',
        actions: 'Actions',
        translations: 'Translations',
        // edit: '',
        // translate: '',
        // copy: '',
        enabled: 'Status',
        delete: ''
      },
      templates: {
        type: '<span class="label label-default">{type}</span>',
        
        type: function(row)
        {

          return "<code>" + row.type + "</code>";
          if(row.type === 'database')
          {
            return '<code>database</code>';
          }else{
            return '<code>file</code>';
          }
        }, 

        html: function(row)
        {
            console.log(row.html);
          if(row.html == 1)
          {
            return '<code>HTML</code>';
          }else{
            return '<code>Plain Text</code>';
          }
        },

        enabled: function(row)
        {
          var status ="Disable";
          if(! row.enabled)
          {
            status ="Activate";
          }
          
          return '<button v-on:click="$parent.togglePageStatus" data-url="content/update/" data-pk="'+ row.id+'" class="btn btn-xs btn-info">'+ status +'</button>';
        },
        actions: function(row){
          

          return "<a href='/admin/content/"+ row.id+ "/copy' class='btn btn-xs btn-success'>copy</a> "+
          "<a href='/admin/content/"+ row.id+ "/edit' class='btn btn-xs btn-success'>edit</a> "+
          "<a href='/admin/content/"+ row.id+ "/translate' class='btn btn-xs btn-success'>translate</a> ";
          
        },

        translations: function(row){
          let $html = '';
          for (var i = row.translation.length - 1; i >= 0; i--) {
            $html += "<a href='/admin/content/edit/"+ row.id + "/translate/" + row.translation[i].id + "' class='btn btn-xs btn-success'>" +row.translation[i].locale + "</a> ";
          };
          return $html;
        },
        // copy: "<a href='/admin/content/{id}/copy' class='btn btn-xs btn-warning'>copy</a>",
        // edit: "<a href='/admin/content/{id}/edit' class='btn btn-xs btn-success'>edit</a>",
        // translate: "<a href='/admin/content/{id}/translate' class='btn btn-xs btn-primary'>translate</a>",
        // translate: "<a href='/admin/content/{id}/translate' class='btn btn-xs btn-primary'>All translates</a>",
        delete: '<button v-on:click="$parent.deletePage" data-url="content/{id}/delete/" data-entry_id="{id}"data-method="post" class="btn btn-xs btn-danger">Delete</button>'
      },
      customFilters: [],
    }
  }
});


