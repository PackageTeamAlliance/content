module.exports = {
	data : function() {
		return {
			delete_uri: ''
		}
	},
	methods: {
		deleteEntry: function(object){
			const _self = this;
			this.delete_uri = object.dataset.url;
			
			swal({
				title: "Warning",
				type: "warning",
				showCancelButton: true,
				cancelButtonText: "Cancel",
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete",
				closeOnConfirm: true
			}, function(confirmed) {
				if (confirmed)
				{
					const _token = document.querySelector('meta[name="_token"').getAttribute('content');

					_self.$http.post(_self.delete_uri, {'_token' : _token})
					.then(
						function(data){
							_self.$children[0].refresh();
						},
						function(error){

						}
					);
				}
			});
			
		}

	}
};