@extends ('backend.layouts.master')

@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/content/content.css')}}">
@stop

@section('page-header')
<h1>{{ trans('pta/content::general.header.index')}}</h1>

@stop

@section('content')
<div class="box">
	<div class="box-body">
		<div id="content-table">
			<v-server-table url="{{{route('content.dashboard.table')}}}" :columns="columns" :options="options"></v-server-table>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{url('module/assets/content/content-index.js')}}"></script>
@stop