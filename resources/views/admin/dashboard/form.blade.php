@extends ('backend.layouts.master')

{{-- Page title --}}
@section('page-title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('pta/content::common.title') }}
@stop

@section('main-panel-title')
@parent
{{{ trans("action.{$mode}") }}}
@stop

@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/content/content.css')}}">
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page --}}
@section('content')
<div class="box">
	<div class="box-body">
		<h3 class="box-title">{{{ trans("action.{$mode}") }}} {{ trans('pta/content::common.title') }}</h3>
		<form role="form" method="POST" class="form-horizontal" id="content-form">

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="content_id" value="{{ $content->id ? : '' }}">

			<div class="box-body">

				<div class="form-group @if($errors->has('name') ) has-error @endif">
					<label class="col-xs-2 control-label">{{{ trans('pta/content::model.general.name') }}}:</label>
					<div class="col-xs-9">
						<input type="text" class="form-control inline-input" name="name" id="name" placeholder="{{{ trans('pta/content::model.general.name') }}}" value="{{{ old('name', $content->name) }}}" data-slugify="#slug">
					</div>

					<span class="help-block">{{{$errors->first('name')}}}</span>
				</div>
				
				<div class="form-group @if($errors->has('slug') ) has-error @endif">
					<label class="col-xs-2 control-label">{{{ trans('pta/content::model.general.slug') }}}:</label>
					<div class="col-xs-9">
						<input type="text" class="form-control inline-input" name="slug" id="slug" placeholder="{{{ trans('pta/content::model.general.slug') }}}" value="{{{ old('slug', $content->slug) }}}">
					</div>

					<span class="help-block">{{{$errors->first('slug')}}}</span>
				</div>

				<div class="form-group @if($errors->has('type') ) has-error @endif">
					<label class="col-xs-2 control-label">{{{ trans('pta/content::model.general.type') }}}:</label>
					<div class="col-xs-9">
						<select class="form-control select" name="type" id="type" v-el:typeselect>
							<option @if(old('type', $content->type) === 'database') selected @endif value="database">Database</option>	
							<option @if(old('type', $content->type) === 'file') selected @endif value="file">File</option>	
						</select>
					</div>

					<span class="help-block">{{{$errors->first('type')}}}</span>
				</div>

				<div class="form-group @if($errors->has('value') ) has-error @endif"  v-show="database">
					<label class="col-xs-2 control-label">{{{ trans('pta/content::model.general.value') }}}:</label>
					<div class="col-xs-9">
						<textarea class="form-control" name="value" id="value" placeholder="{{{ trans('pta/content::model.general.value') }}}">{{{ old('value', $content->value) }}}</textarea>	        	
					</div>

					<span class="help-block">{{{$errors->first('value')}}}</span>
				</div>

				<div class="form-group @if($errors->has('type') ) has-error @endif" v-show="!database">
					<label class="col-xs-2 control-label">{{{ trans('pta/content::model.general.file') }}}:</label>
					<div class="col-xs-9">
						<select class="form-control select" name="file" id="file" v-el:file>
							<option value="">---</option>
							@foreach($files as $p)
							<option value="{{{ $p->getRelativePathName() }}}" @if( old('file', $content->file) === $p->getRelativePathName() )  ) selected @endif> {{{ $p->getRelativePathName() }}}</option>
							@endforeach
						</select>
					</div>

					<span class="help-block">{{{$errors->first('type')}}}</span>
				</div> 

				<div class="form-group @if($errors->has('html') ) has-error @endif">

					<label class="col-xs-2 control-label">{{{ trans('pta/content::model.general.html') }}}</label>

					<div class="col-xs-9">
						<select class="form-control select" name="html" id="html" v-el:html>
							<option @if(old('html', 	$content->html)) selected @endif value="1">HTML/Markup</option>	
							<option @if(old('html', ! $content->html)) selected @endif value="0">Plain Text</option>
						</select>
					</div>
					
					<span class="help-block">{{{$errors->first('html')}}}</span>
				</div>

				<div class="form-group @if($errors->has('enabled') ) has-error @endif">

					<label class="col-xs-2 control-label" for="enabled">{{{ trans('pta/content::model.general.active') }}}</label>

					<div class="col-xs-9">
						<select class="form-control select" name="enabled" id="enabled" v-el:enabled>
							<option @if(old('enabled', 	$content->enabled)) selected @endif value="1">Published</option>	
							<option @if(old('enabled', !$content->enabled)) selected @endif value="0">Draft</option>
						</select>
					</div>
					
					<span class="help-block">{{{$errors->first('html')}}}</span>
				</div>


				<div class="form-group">

					<label class="col-xs-2 control-label" for="submit">{{{ trans('pta/content::general.actions.save') }}}</label>

					<div class="col-xs-9">
						<button class="btn btn-success"><i class="fa fa-save"></i> {{{ trans('pta/content::general.actions.save') }}}</button>
					</div>

				</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				
			</div><!-- /.box-footer -->
		</form>
	</div>
</div>
@stop

@section('vue-id')id="content-form"@stop

@section('scripts')
<script src="{{url('module/assets/content/content.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
<script>
	var contentForm = new Vue({ 
		el: '#content-form',

		data: {
			database: @if( old('type', $content->type) === 'database' || old('type', $content->type) === null ) true @else false @endif ,
			type_select: null
		},
		ready: function() {
			
			$( this.$els.html ).selectize({
				create: true,
				sortField: 'text'
			});

			$( this.$els.file ).selectize({
				create: true,
				sortField: 'text'
			});

			$( this.$els.enabled ).selectize({
				create: true,
				sortField: 'text'
			});
			
			var select = $( this.$els.typeselect ).selectize({
				create: true,
				sortField: 'text'
			});

			this.type_select = select[0].selectize;
			this.type_select.on('change', this.toggleType);
			
		},
		methods: {

			toggleType: function (e) {
				var value = this.type_select.getValue();
				if(value === 'database')
				{

					this.$set('database', true );
				}else{
					this.$set('database', false );
				}

			}
		}
	});


	$(function () {
		$('#value').summernote({
			height: 300,
			tabsize: 2,
			lang: '{{app()->getLocale()}}',
			'fontName':  'Source Sans Pro'
		});

		$('#name').on('keyup', function (e) {

			if (String.prototype.slugify) {
				var input = $(this).data('slugify');

				var slug = $(this).val().slugify();

				$(input).val(slug);
			}
		});
	});
</script>


@stop