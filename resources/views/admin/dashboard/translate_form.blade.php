@extends ('backend.layouts.master')


@section('main-panel-title')
@parent
{{{ trans("action.{$mode}") }}}
@stop

@section('after-styles-end')
<link rel="stylesheet" href="{{url('module/assets/content/content.css')}}">
<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
@stop

{{-- Inline scripts --}}
@section('scripts')
<script src="{{url('module/assets/content/content.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
<script type="text/javascript">
	$(function()
	{
		$('#value').summernote({
			height: 300,
			tabsize: 2,
			lang: '{{app()->getLocale()}}',
			'fontName':  'Source Sans Pro'
		});

		$('.select').selectize({

		});

		var $select = $("#locale").selectize({
			create: true,
			sortField: 'text'
		});

		function make_conent_id (locale) {

			return  "{{$content->id}}_" + locale;
		}


		var selectizeControl = $select[0].selectize

		selectizeControl.on('change', function(){
			var content_locale = selectizeControl.getValue();

			$('#content_locale').val( make_conent_id(content_locale));

		})

	});
</script>
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page --}}
@section('content')
{{-- Form --}}
<div class="box">
	<div class="box-body">
		<h3 class="box-title">{{{ trans("action.{$mode}") }}} {{ trans('pta/content::common.title') }}</h3>
		<form role="form" method="POST" class="form-horizontal">
			<div class="box-body">

				{{-- Form: CSRF Token --}}
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="content_locale" value="{{ old('content_locale', $translation->content_locale ? : '' )}}" id="content_locale">
				<input type="hidden" name="content_id" value="{{{$content->id}}}">
				<input type="hidden" name="translation_id" value="{{{$translation->id ? : ''}}}">

				<fieldset>
					<div class="form-group">
						<label class="col-xs-2 control-label" for="slug">{{{ trans('pta/content::model.general.name') }}}</label>
						<div class="col-xs-9">
							<input type="text" class="form-control inline-input" value="{{{ old('slug', $content->name) }}}" disabled>
						</div>
					</div>

					<div class="form-group @if($errors->has('locale') ) has-error @endif">

						<label class="col-xs-2 control-label" for="locale">{{{ trans('pta/content::model.general.locale') }}}</label>
						<div class="col-xs-9">

							<select class="form-control select" name="locale" id="locale">
								<option value="">---</option>
								@foreach($locale as $l)
								<option @if(old('locale', $translation->locale) === $l['short_name'] ) selected @endif value="{{{$l['short_name']}}}">{{{$l['name']}}}</option>
								@endforeach

							</select>
							<span class="help-block">{{{$errors->first('locale')}}}</span>
						</div>



					</div>

					<div class="form-group @if($errors->has('value') ) has-error @endif">

						<label class="col-xs-2 control-label" for="value">{{{ trans('pta/content::model.general.value') }}}</label>
						<div class="col-xs-9">
							<textarea class="form-control" name="value" id="value" placeholder="{{{ trans('pta/content::model.general.value') }}}">{{old('value', $translation->value) }}</textarea>

						<span class="help-block">{{{$errors->first('value')}}}</span>
					</div>



				</div>

				<div class="form-group">

					<label class="col-xs-2 control-label" for="submit">{{{ trans('action.save') }}}</label>

					<div class="col-xs-9">
						<button class="btn btn-success">

							<i class="fa fa-save"></i> {{{ trans('action.save') }}}

						</button>
					</div>

				</div>

			</fieldset>
		</div>
	</form>
</div>

{{-- End col-md-12 --}}
@stop
