@permission(config('content.permissions.view.name'))
<li class="{{ Active::pattern('admin/content*') }} treeview">
	<a href="#">
		<span>{{ trans('pta/content::menu.main') }}</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu {{ Active::pattern('admin/content*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/content*', 'display: block;') }}">
		<li class="{{ Active::pattern('admin/content') }}">
			<a href="{!! url('admin/content') !!}"><i class="fa fa-circle-o"></i>{{ trans('pta/content::menu.dashboard') }}</a>
		</li>
		<li class="{{ Active::pattern('admin/content/create') }}">
			<a href="{!! url('admin/content/create') !!}"><i class="fa fa-circle-o"></i>{{ trans('pta/content::menu.create') }}</a>
		</li>
	</ul>
</li>
@endauth